drop table if exists product;
drop table if exists users;

create table product
(
    id           bigserial primary key,
    product_name varchar(20),
    price        int,
    count        int check ( count >= 0 ),
    color        varchar(20)
);

create table users
(
    id         bigserial primary key,
    first_name varchar(20),
    last_name  varchar(20),
    age        integer check ( age > 18 and age < 120),
    email      varchar(30),
    password   varchar(30)
);