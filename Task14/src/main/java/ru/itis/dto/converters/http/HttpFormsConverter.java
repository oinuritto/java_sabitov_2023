package ru.itis.dto.converters.http;

import jakarta.servlet.http.HttpServletRequest;
import ru.itis.dto.ProductDto;

public class HttpFormsConverter {
    public static ProductDto productDtoFrom(HttpServletRequest request) {
        return ProductDto.builder()
                .productName(request.getParameter("productName"))
                .price(Integer.parseInt(request.getParameter("price")))
                .count(Integer.parseInt(request.getParameter("count")))
                .color(request.getParameter("color"))
                .build();
    }

//    public static SignUpDto signUpDtoFrom(HttpServletRequest request) {
//        return SignUpDto.builder()
//                .firstName(request.getParameter("firstName"))
//                .lastName(request.getParameter("lastName"))
//                .email(request.getParameter("email"))
//                .password(request.getParameter("password"))
//                .build();
//    }
}
