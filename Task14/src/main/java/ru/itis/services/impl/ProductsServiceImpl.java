package ru.itis.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import ru.itis.dto.ProductDto;
import ru.itis.models.Product;
import ru.itis.repositories.ProductsRepository;
import ru.itis.services.ProductsService;

import java.util.ArrayList;
import java.util.List;

import static ru.itis.dto.ProductDto.from;

@Service
@RequiredArgsConstructor
public class ProductsServiceImpl implements ProductsService {
    private final ProductsRepository productsRepository;

    @Override
    public List<ProductDto> addProduct(ProductDto productData) {
        Product product = Product.builder()
                .productName(productData.getProductName())
                .price(productData.getPrice())
                .count(productData.getCount())
                .color(productData.getColor())
                .build();

        productsRepository.save(product);
        return from(productsRepository.findAll());
    }

    @Override
    public List<ProductDto> getAllProducts() {
        return from(productsRepository.findAll());
    }

    @Override
    public boolean deleteProduct(Long id) {
        return productsRepository.delete(id) != 0;
    }

    @Override
    public List<ProductDto> getProductLikeName(String productName) {
        return from(productsRepository.findProductsLikeName(productName.toLowerCase()));
    }

    @Override
    public List<ProductDto> getAllProductsSortedById() {
        return from(productsRepository.findAllOrderByIdDesc());
    }

    @Override
    public List<ProductDto> getAllProductsSortedByName() {
        return from(productsRepository.findAllOrderByNameDesc());
    }

    @Override
    public List<ProductDto> getAllProductsSortedByCount() {
        return from(productsRepository.findAllOrderByCountDesc());
    }

    @Override
    public List<ProductDto> getAllProductsSortedByPrice() {
        return from(productsRepository.findAllOrderByPriceDesc());
    }

    @Override
    public List<ProductDto> getAllProductsSortedBy(String sortBy) {
        List<ProductDto> products;
        switch (sortBy) {
            case ("id") -> products = getAllProductsSortedById();
            case ("productName") -> products = getAllProductsSortedByName();
            case ("price") -> products = getAllProductsSortedByPrice();
            case ("count") -> products = getAllProductsSortedByCount();
            default -> products = getAllProducts();
        }
        return products;
    }
}
