package ru.itis.services;

import ru.itis.dto.ProductDto;
import ru.itis.models.Product;

import java.util.List;

public interface ProductsService {
    List<ProductDto> addProduct(ProductDto productData);
    List<ProductDto> getAllProducts();
    boolean deleteProduct(Long id);

    List<ProductDto> getProductLikeName(String productName);

    List<ProductDto> getAllProductsSortedById();

    List<ProductDto> getAllProductsSortedByName();

    List<ProductDto> getAllProductsSortedByCount();

    List<ProductDto> getAllProductsSortedByPrice();

    List<ProductDto> getAllProductsSortedBy(String sortBy);
}
