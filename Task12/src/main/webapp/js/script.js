function searchProducts(query) {
    return fetch('/app/liveSearchProduct?query=' + query)
        .then((response) => {
            return response.json()
        }).then((products) => {
            fillTable(products)
        })
}

function fillTable(products) {
    let table = document.getElementById("productsTable");

    table.innerHTML = "<tr>\n" +
        "      <th>id</th>\n" +
        "      <th>Product name</th>\n" +
        "      <th>Price</th>\n" +
        "      <th>Count</th>\n" +
        "      <th>Color</th>\n" +
        "    </tr>";

    for (let i = 0; i < products.length; i++) {
        let row = table.insertRow(-1);
        let idCell = row.insertCell(0);
        let productNameCell = row.insertCell(1);
        let priceCell = row.insertCell(2);
        let countCell = row.insertCell(3);
        let colorCell = row.insertCell(4);

        idCell.innerHTML = products[i].id;
        productNameCell.innerHTML = products[i].productName;
        priceCell.innerHTML = products[i].price;
        countCell.innerHTML = products[i].count;
        colorCell.innerHTML = products[i].color;
    }
}

function addProduct(productName, price, count, color) {
    let body = {
        "productName": productName,
        "price": price,
        "count": count,
        "color": color
    };

    fetch('/app/products', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
    }).then((response) => response.json())
        .then((products) => fillTable(products))
        .catch((error) => {
            alert(error)
        })
}
