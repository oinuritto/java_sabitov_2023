package ru.kpfu.itis.oinuritto.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static ru.kpfu.itis.oinuritto.constants.Paths.SIGN_IN_PATH;

@WebServlet(SIGN_IN_PATH)
public class SignInServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/signIn.html").forward(req, resp);
    }
}
