package ru.kpfu.itis.oinuritto.security;

public interface AuthenticationManager {
    boolean authenticate(String email, String password);
}
