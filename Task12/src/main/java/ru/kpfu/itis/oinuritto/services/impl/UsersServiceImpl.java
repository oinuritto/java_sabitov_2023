package ru.kpfu.itis.oinuritto.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.oinuritto.dto.SignUpDto;
import ru.kpfu.itis.oinuritto.dto.UserDto;
import ru.kpfu.itis.oinuritto.models.User;
import ru.kpfu.itis.oinuritto.repositories.UsersRepository;
import ru.kpfu.itis.oinuritto.services.UsersService;

import java.util.List;

import static ru.kpfu.itis.oinuritto.dto.UserDto.from;

@RequiredArgsConstructor
@Service
public class UsersServiceImpl implements UsersService {

    private final UsersRepository usersRepository;

    @Override
    public void signUp(SignUpDto signUpData) {
            User user = User.builder()
                    .firstName(signUpData.getFirstName())
                    .lastName(signUpData.getLastName())
                    .email(signUpData.getEmail())
                    .password(signUpData.getPassword())
                    .build();

            usersRepository.save(user);
    }

    @Override
    public List<UserDto> getAllUsers() {
        return from(usersRepository.findAll());
    }

    @Override
    public List<User> getAllUsersByAge(int ageFrom, int ageTo) {
        return usersRepository.findAllByAgeInRangeOrderByIdDesc(ageFrom, ageTo);
    }
}
