package ru.kpfu.itis.oinuritto.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.ApplicationContext;
import ru.kpfu.itis.oinuritto.dto.ProductDto;
import ru.kpfu.itis.oinuritto.dto.converters.http.HttpFormsConverter;
import ru.kpfu.itis.oinuritto.services.ProductsService;
import ru.kpfu.itis.oinuritto.utils.HtmlGenerator;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import static ru.kpfu.itis.oinuritto.constants.Paths.*;

@WebServlet(urlPatterns = {PRODUCTS_PATH, ADD_PRODUCT_PATH}, loadOnStartup = 1)
public class ProductsServlet extends HttpServlet {
    private ProductsService productsService;
    private HtmlGenerator htmlGenerator;
    private ObjectMapper objectMapper;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.productsService = context.getBean(ProductsService.class);
        this.htmlGenerator = context.getBean(HtmlGenerator.class);
        this.objectMapper = context.getBean(ObjectMapper.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getRequestURI().equals(APPLICATION_PREFIX + PRODUCTS_PATH)) {
            List<ProductDto> products;

//            products = productsService.getAllProducts();

            products = getProductsList(req);

            resp.setContentType("text/html");
            PrintWriter writer = resp.getWriter();

            String html = htmlGenerator.getHtmlForProductsDto(products);

            writer.println(html);
        }
        else {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getRequestURI().equals(APPLICATION_PREFIX + ADD_PRODUCT_PATH)) {
            ProductDto productDto = HttpFormsConverter.productDtoFrom(req);

            productsService.addProduct(productDto);
            resp.sendRedirect(APPLICATION_PREFIX + PRODUCTS_PATH);
        } else if (req.getRequestURI().equals(APPLICATION_PREFIX + PRODUCTS_PATH)) {
            String body = req.getReader().readLine();
            ProductDto productData = objectMapper.readValue(body, ProductDto.class);
            productsService.addProduct(productData);
            List<ProductDto> products = productsService.getAllProducts();
            String jsonResponse = objectMapper.writeValueAsString(products);
            resp.setContentType("application/json");
            resp.getWriter().write(jsonResponse);

        } else {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }

    }

    private Cookie getCookieByName(String name, HttpServletRequest req) {
        Cookie[] cookies = req.getCookies();
        Cookie cookie = null;
        if (cookies != null) {
            for (Cookie c : cookies) {
                if (name.equals(c.getName())) {
                    cookie = c;
                    return cookie;
                }
            }
        }
        return null;
    }

    private List<ProductDto> getProductsList(HttpServletRequest req) {
        List<ProductDto> products;
        Cookie sortByCookie = getCookieByName("sortBy", req);
        String sortBy = "";
        if (sortByCookie != null) {
            sortBy = sortByCookie.getValue().trim();
        }
        switch (sortBy)  {
            case ("id") -> products = productsService.getAllProductsSortedById();
            case ("productName") -> products = productsService.getAllProductsSortedByName();
            case ("price") -> products = productsService.getAllProductsSortedByPrice();
            case ("count") -> products = productsService.getAllProductsSortedByCount();
            default -> products = productsService.getAllProducts();
        }
        return products;
    }
}
