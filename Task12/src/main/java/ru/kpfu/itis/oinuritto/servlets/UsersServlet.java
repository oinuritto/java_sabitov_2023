package ru.kpfu.itis.oinuritto.servlets;

import org.springframework.context.ApplicationContext;
import ru.kpfu.itis.oinuritto.dto.SignUpDto;
import ru.kpfu.itis.oinuritto.dto.converters.http.HttpFormsConverter;
import ru.kpfu.itis.oinuritto.services.UsersService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static ru.kpfu.itis.oinuritto.constants.Paths.*;

@WebServlet(name = "usersServlet", urlPatterns = {SIGN_UP_PATH}, loadOnStartup = 1)
public class UsersServlet extends HttpServlet {
    private UsersService usersService;

    @Override
    public void init(ServletConfig config) {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.usersService = context.getBean(UsersService.class);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (request.getRequestURI().equals(APPLICATION_PREFIX + SIGN_UP_PATH)) {
            SignUpDto signUpData = HttpFormsConverter.signUpDtoFrom(request);
            usersService.signUp(signUpData);
            response.sendRedirect(APPLICATION_PREFIX + PROFILE_PAGE);
        }
        else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }
}
