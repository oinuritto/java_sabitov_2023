package ru.kpfu.itis.oinuritto.services;

import ru.kpfu.itis.oinuritto.dto.ProductDto;

import java.util.List;

public interface SearchService {
    List<ProductDto> searchProducts(String query);
}
