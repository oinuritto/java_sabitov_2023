package ru.kpfu.itis.oinuritto.services;

import ru.kpfu.itis.oinuritto.dto.ProductDto;
import ru.kpfu.itis.oinuritto.models.Product;

import java.util.List;

public interface ProductsService {
    boolean addProduct(ProductDto productData);
    List<ProductDto> getAllProducts();
    boolean deleteProduct(Long id);

    List<Product> getProductLikeName(String productName);

    List<ProductDto> getAllProductsSortedById();

    List<ProductDto> getAllProductsSortedByName();

    List<ProductDto> getAllProductsSortedByCount();

    List<ProductDto> getAllProductsSortedByPrice();
}
