package ru.kpfu.itis.oinuritto.dto.converters.http;

import ru.kpfu.itis.oinuritto.dto.ProductDto;
import ru.kpfu.itis.oinuritto.dto.SignUpDto;

import javax.servlet.http.HttpServletRequest;

public class HttpFormsConverter {
    public static ProductDto productDtoFrom(HttpServletRequest request) {
        return ProductDto.builder()
                .productName(request.getParameter("productName"))
                .price(Integer.parseInt(request.getParameter("price")))
                .count(Integer.parseInt(request.getParameter("count")))
                .color(request.getParameter("color"))
                .build();
    }

    public static SignUpDto signUpDtoFrom(HttpServletRequest request) {
        return SignUpDto.builder()
                .firstName(request.getParameter("firstName"))
                .lastName(request.getParameter("lastName"))
                .email(request.getParameter("email"))
                .password(request.getParameter("password"))
                .build();
    }
}
