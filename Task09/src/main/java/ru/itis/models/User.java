package ru.itis.models;

import lombok.*;
import ru.itis.annotations.ColumnName;
import ru.itis.annotations.TableName;

@AllArgsConstructor
@Setter
@Getter
@Builder
@ToString
@TableName("account")
public class User {
    @ColumnName(columnName = "id", primary = true, identity = true)
    private Long id;
    @ColumnName(columnName = "first_name", maxLength = 25)
    private String firstName;
    @ColumnName(columnName = "last_name")
    private String lastName;
    @ColumnName(columnName = "is_worker", defaultBoolean = true)
    private boolean isWorker;

}
