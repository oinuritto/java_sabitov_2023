package ru.itis;

import ru.itis.models.User;

public class Main {
    public static void main(String[] args) {
        User user = User.builder()
                .firstName("Yasha")
                .lastName("Ivanov")
                .isWorker(false)
                .build();

        SqlGenerator sqlGenerator = new SqlGenerator();
        System.out.println(sqlGenerator.createTable(user.getClass()));
        System.out.println(sqlGenerator.insert(user));
    }
}
