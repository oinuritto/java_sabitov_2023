package ru.itis;

import ru.itis.annotations.ColumnName;
import ru.itis.annotations.TableName;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class SqlGenerator {
    public <T> String createTable(Class<T> entityClass) {
        StringBuilder createTableSb = new StringBuilder("create table ");

        TableName tableName = entityClass.getAnnotation(TableName.class);
        createTableSb.append(tableName.value()).append(" (\n");

        Field[] fields = entityClass.getDeclaredFields();
        for (Field field : fields) {
            ColumnName columnName = field.getAnnotation(ColumnName.class);
            createTableSb.append("\t").append(columnName.columnName()).append(" ");

            switch (field.getType().getSimpleName()) {
                case "Long" -> createTableSb.append("bigint ");
                case "int", "Integer" -> createTableSb.append("int ");
                case "String" -> {
                    createTableSb.append("varchar");
                    if (columnName.maxLength() != -1) {
                        createTableSb.append("(").append(columnName.maxLength()).append(") ");
                    } else {
                        createTableSb.append(" ");
                    }
                }
                case "boolean", "Boolean" -> createTableSb.append("boolean ");
            }
            createTableSb.append(columnName.primary() ? "primary key " : "");
            createTableSb.append(columnName.defaultBoolean() ? "default true " : "");
            createTableSb.append(columnName.identity() ? "GENERATED ALWAYS AS IDENTITY " : "");

            createTableSb.append(",\n");

        }
        createTableSb.delete(createTableSb.length() - 3, createTableSb.length()).append("\n);");

        return createTableSb.toString();

    }

    public String insert(Object entity) {
        StringBuilder insertSb = new StringBuilder("insert into ");

        TableName tableName = entity.getClass().getAnnotation(TableName.class);
        insertSb.append(tableName.value()).append("(");

        Field[] fields = entity.getClass().getDeclaredFields();
        ArrayList<String> values = new ArrayList<>();
        for (Field field: fields) {
            ColumnName columnName = field.getAnnotation(ColumnName.class);

            // check if column is auto-generating, not add to insert if true
            if (columnName.identity() || columnName.primary()) {
                continue;
            }

            insertSb.append(columnName.columnName()).append(", ");

            try {
                field.setAccessible(true);
                if (field.getType().equals(String.class)) {
                    values.add("'" + field.get(entity) + "'");
                } else {
                    values.add(String.valueOf(field.get(entity)));
                }
                field.setAccessible(false);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
        String strValues = values.toString();
        strValues = strValues.substring(1, strValues.length() - 1);

        insertSb.delete(insertSb.length() - 2, insertSb.length()).append(")\n");
        insertSb.append("\tvalues(");
        insertSb.append(strValues).append(");");

        return insertSb.toString();
    }
}
