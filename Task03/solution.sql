-- 1
Select model, speed, hd
from PC
where (price < 500)

-- 2
Select distinct maker
from Product
where (type = 'Printer')

-- 3
Select model, ram, screen
from Laptop
where (price > 1000)

-- 4
Select * 
from Printer
where (color = 'y')

-- 5
Select model, speed, hd
from PC
where ((cd = '12x' or cd = '24x') and price < 600)

-- 6
select distinct p.maker, l.speed
from Product p, Laptop l
where (p.model = l.model and l.hd >= 10)

-- 7
Select pr.model, pc.price
from Product pr, PC pc
where (pr.model = pc.model and pr.maker = 'B')
UNION
Select pr.model, l.price
from Product pr, Laptop l
where (pr.model = l.model and pr.maker = 'B')
UNION
Select pr.model, Printer.price
from Product pr, Printer
where (pr.model = Printer.model and pr.maker = 'B')

-- 8
select distinct pr.maker 
from Product pr 
where (pr.type = 'PC')
except
select distinct pr.maker 
from Product pr 
where (pr.type = 'Laptop')

-- 9
Select distinct pr.maker
from Product pr
where pr.model in (
select pc.model
from PC pc
where (pc.speed >= 450))

-- 10
Select distinct model, price
from Printer
where price = (
select max(price)
from Printer)

-- 11
Select avg(speed)
from PC

-- 12
select avg(speed)
from Laptop
where price > 1000

-- 13
Select avg(speed)
from PC
where model in (
select model
from Product
where maker = 'A')

-- 14
Select distinct sh.class, sh.name, cl.country 
from ships sh, classes cl 
where (sh.class = cl.class and cl.numGuns >= 10)

-- 15
select hd 
from PC
group by hd
having count(hd) >= 2

-- 16
select distinct pc1.model, pc2.model, pc1.speed, pc1.ram 
from PC pc1, PC pc2 
where (pc1.speed = pc2.speed and
pc1.ram = pc2.ram and not (pc1.model = pc2.model) 
and pc1.model > pc2.model)

-- 17
Select distinct pr.type, pr.model, l.speed
from Product pr, Laptop l
where (l.speed < all( 
select speed
from PC) and pr.model = l.model)

-- 18
Select distinct prd.maker, pr.price 
from Product prd, Printer pr 
where pr.model = prd.model 
and pr.price = (select min(price) from Printer where color = 'y') 
and pr.color = 'y'

-- 19
Select prd.maker, avg(l.screen)
from Product prd, Laptop l
where prd.model = l.model
group by prd.maker

-- 20
Select distinct prd.maker, count(distinct prd.model) as count
from Product prd
where prd.type = 'PC'
group by prd.maker
having count(prd.model) >= 3

-- 21
Select prd.maker, max(pc.price)
from Product prd, PC pc
where prd.model = pc.model
group by prd.maker

-- 22
Select pc.speed, avg(pc.price)
from PC pc
where pc.speed > 600
group by pc.speed

-- 23
Select distinct maker
from product prd 
join PC pc on prd.model = pc.model
where speed >=750 and maker in
(select maker
from product prd 
join laptop l on prd.model=l.model
where speed >= 750)

-- 24
Select model
from (
 select model, price
 from pc
 UNION
 select model, price
 from Laptop
 UNION
 select model, price
 from Printer
) t1
where price = (
 select max(price)
 from (
  select price
  from pc
  UNION
  select price
  from Laptop
  UNION
  select price
  from Printer
  ) t2
 )

-- 25
Select distinct prd.maker
from Product prd
where prd.model in (
select model 
from PC 
where ram = 
(select min(ram)
from pc)
and speed = 
(select max(speed)
from PC
where ram = 
(select min(ram)
from PC)))
and prd.maker in 
(select maker
from Product
where type = 'Printer')

-- 26
Select AVG(price)
from (
select code, price, model
from pc
where model IN (
select model
from product
where maker='A'
)
UNION
select code, price, model
from laptop
where model IN (
select model
from product
where maker='A'
)
) a

-- 27
Select prd.maker, avg(pc.hd) as AVG
from Product prd, PC pc
where prd.model = pc.model
and prd.maker in (select prd.maker from Product prd where prd.type = 'Printer')
group by prd.maker

-- 28
select count(*) from
(select maker 
from product 
group by maker 
having count(*) = 1) a

-- 29
Select ino.point, ino.date, inc, out
from Income_o ino
left join Outcome_o outc on ino.point = outc.point and ino.date = outc.date
UNION
Select outc.point, outc.date, inc, out
from Outcome_o outc
left join Income_o ino on ino.point = outc.point and ino.date = outc.date

-- 30
select point, date, SUM(sum_out), SUM(sum_inc)
from (select point, date, SUM(inc) as sum_inc, null as sum_out 
from Income 
Group by point, date
Union
select point, date, null as sum_inc, SUM(out) as sum_out 
from Outcome 
Group by point, date ) as t
group by point, date 
order by point

-- 31
Select class, country
from Classes
where bore >= 16

-- 32
Select country, cast(avg((power(bore,3)/2)) as numeric(6,2)) as AVG
from (
select country, cl.class, bore, name 
from classes cl
left join ships sh on cl.class = sh.class
union all
select distinct country, class, bore, ship 
from classes cl 
left join outcomes outc on cl.class = outc.ship
where ship = class and ship not in (
select name from ships)
) a
where name IS NOT NULL group by country

-- 33
Select ship
from Outcomes o
where result = 'sunk' and o.battle = 'North Atlantic'

-- 34
Select distinct sh.name
from Ships sh
where sh.launched >= 1922 and sh.class in (
select class 
from Classes
where displacement > 35000 and type = 'bb')

-- 35
Select model, type
from product
where lower(model) not LIKE '%[^a-z]%'
OR model not LIKE '%[^0-9]%'

-- 36
Select name 
from ships 
where class = name
union
select ship as name 
from classes cl, outcomes out
where cl.class = out.ship

-- 37
Select cl.class
from classes cl
left join (
select class, name
from ships
UNION
select ship, ship
from outcomes
) as s on s.class = cl.class
GROUP BY cl.class
HAVING count(s.name) = 1

-- 38
Select country
from classes
group by country
HAVING count(distinct type) = 2

-- 39
Select distinct o.ship
From Outcomes o, Battles b
where o.battle = b.name and o.ship in
(Select o1.ship
from Outcomes o1, Battles b1
where o1.battle = b1.name and o1.result = 'damaged' and b.date > b1.date)

-- 40
Select maker, min(type)
from product
group by maker
having count(distinct type) = 1 and count(model) > 1
