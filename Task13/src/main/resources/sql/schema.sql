drop table if exists product;
drop table if exists users;

create table product
(
    id           bigserial primary key,
    product_name varchar(20),
    price        int,
    count        int check ( count >= 0 ),
    color        varchar(20)
);

create table users
(
    id         bigserial primary key,
    first_name varchar(20),
    last_name  varchar(20),
    age        integer check ( age > 18 and age < 120),
    email      varchar(30),
    password   varchar(30)
);

create table file_info (
                           id bigserial primary key,
                           original_file_name varchar(1000),
                           storage_file_name varchar(100),
                           size bigint,
                           mime_type varchar(50),
                           description varchar(1000)
);

alter table users add column img_id bigint;
alter table users add foreign key(img_id) references file_info(id);