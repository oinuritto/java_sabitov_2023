insert into product(product_name, price, count, color)
VALUES ('Rag', 100, 5000, 'Yellow'),
       ('Bucket', 60, 400, 'Black'),
       ('Iphone 12', 60000, 100, 'Black'),
       ('Paper', 5, 10000, 'White');