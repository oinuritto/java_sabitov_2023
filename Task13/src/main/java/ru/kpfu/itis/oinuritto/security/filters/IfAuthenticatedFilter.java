package ru.kpfu.itis.oinuritto.security.filters;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static ru.kpfu.itis.oinuritto.constants.Paths.*;

@WebFilter(urlPatterns = {SIGN_IN_PAGE, SIGN_IN_PATH})
public class IfAuthenticatedFilter extends HttpFilter {
    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpSession session = req.getSession(false);

        if (session != null) {
            Boolean authenticated = (Boolean) session.getAttribute("authenticated");
            if (authenticated != null && authenticated) {
                res.sendRedirect(APPLICATION_PREFIX + PROFILE_PATH);
                return;
            }
        }
        chain.doFilter(req, res);
    }
}
