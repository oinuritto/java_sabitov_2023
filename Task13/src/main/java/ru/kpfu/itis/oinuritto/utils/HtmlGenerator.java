package ru.kpfu.itis.oinuritto.utils;

import org.springframework.stereotype.Component;
import ru.kpfu.itis.oinuritto.dto.ProductDto;
import ru.kpfu.itis.oinuritto.models.Product;

import java.util.List;

@Component
public class HtmlGenerator {
    public String getHtmlForProductsDto(List<ProductDto> products) {
        StringBuilder html = new StringBuilder();

        html.append("""
                <!DOCTYPE html>
                <html>
                <head>
                \t<meta charset="utf-8">
                \t<meta name="viewport" content="width=device-width, initial-scale=1">
                \t<title>Products</title>
                </head>
                <body>
                <form action="/app/products/sortChange" method="post">
                        <select name="sortBySelect">
                          <option value="">Default</option>
                          <option value="id">ID</option>
                          <option value="productName">Name</option>
                          <option value="price">Price</option>
                          <option value="count">Count</option>
                        </select>
                      <input type="submit" value="Sort" />
                </form>
                <table>
                \t<tr>
                \t\t<th>ID</th>
                \t\t<th>Product Name</th>
                \t\t<th>Price</th>
                \t\t<th>Count</th>
                \t\t<th>Color</th>
                \t</tr>""");

        for (ProductDto productDto : products) {
            html.append("<tr>\n");
            html.append("<td>").append(productDto.getId()).append("</td>\n");
            html.append("<td>").append(productDto.getProductName()).append("</td>\n");
            html.append("<td>").append(productDto.getPrice()).append("</td>\n");
            html.append("<td>").append(productDto.getCount()).append("</td>\n");
            html.append("<td>").append(productDto.getColor()).append("</td>\n");
            html.append("</tr>\n");
        }

        html.append("""
                </table>
                </body>
                </html>""");
        return html.toString();
    }

    public String getHtmlForProducts(List<Product> products) {
        StringBuilder html = new StringBuilder();

        html.append("""
                <!DOCTYPE html>
                <html>
                <head>
                \t<meta charset="utf-8">
                \t<meta name="viewport" content="width=device-width, initial-scale=1">
                \t<title>Products</title>
                </head>
                <body>
                <table>
                \t<tr>
                \t\t<th>ID</th>
                \t\t<th>Product Name</th>
                \t\t<th>Price</th>
                \t\t<th>Count</th>
                \t\t<th>Color</th>
                \t</tr>""");

        for (Product product : products) {
            html.append("<tr>\n");
            html.append("<td>").append(product.getId()).append("</td>\n");
            html.append("<td>").append(product.getProductName()).append("</td>\n");
            html.append("<td>").append(product.getPrice()).append("</td>\n");
            html.append("<td>").append(product.getCount()).append("</td>\n");
            html.append("<td>").append(product.getColor()).append("</td>\n");
            html.append("</tr>\n");
        }

        html.append("""
                </table>
                </body>
                </html>""");
        return html.toString();
    }
}
