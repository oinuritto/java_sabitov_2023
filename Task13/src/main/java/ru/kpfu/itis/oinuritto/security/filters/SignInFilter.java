package ru.kpfu.itis.oinuritto.security.filters;

import org.springframework.context.ApplicationContext;
import ru.kpfu.itis.oinuritto.security.AuthenticationManager;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static ru.kpfu.itis.oinuritto.constants.Paths.*;

@WebFilter(SIGN_IN_PATH)
public class SignInFilter implements Filter {

    private AuthenticationManager authenticationManager;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        ApplicationContext context = (ApplicationContext) filterConfig.getServletContext().getAttribute("springContext");
        this.authenticationManager = context.getBean(AuthenticationManager.class);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest)servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        if (request.getMethod().equals("POST")) {
            String email = request.getParameter("email");
            String password = request.getParameter("password");
            if (authenticationManager.authenticate(email, password)) {
                HttpSession session = request.getSession(true);
                session.setAttribute("authenticated", true);
                session.setAttribute("email", email);

                // перебрасываем на страницу, с которой был запрос на signIn, если не null
                System.out.println(AuthenticationFilter.fromURI + "------------");
                if (AuthenticationFilter.fromURI != null) {
                    response.sendRedirect(AuthenticationFilter.fromURI);
                } else {
                    response.sendRedirect(APPLICATION_PREFIX + PROFILE_PATH);
                }
            } else {
                response.sendRedirect(APPLICATION_PREFIX + SIGN_IN_PATH + "?error");
            }
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }
}
