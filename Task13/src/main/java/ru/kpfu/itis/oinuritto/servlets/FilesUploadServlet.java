package ru.kpfu.itis.oinuritto.servlets;

import org.springframework.context.ApplicationContext;
import ru.kpfu.itis.oinuritto.dto.FileDto;
import ru.kpfu.itis.oinuritto.models.User;
import ru.kpfu.itis.oinuritto.repositories.FilesRepository;
import ru.kpfu.itis.oinuritto.services.FilesService;
import ru.kpfu.itis.oinuritto.services.UsersService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static ru.kpfu.itis.oinuritto.constants.Paths.*;

@WebServlet(FILES_UPLOAD_PATH)
@MultipartConfig
public class FilesUploadServlet extends HttpServlet {

    private FilesService filesService;
    private UsersService usersService;
    private FilesRepository filesRepository;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.filesService = context.getBean(FilesService.class);
        this.usersService = context.getBean(UsersService.class);
        this.filesRepository = context.getBean(FilesRepository.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getServletContext().getRequestDispatcher("/setProfileImage.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        String description = (new BufferedReader(
//                new InputStreamReader(request.getPart("description").getInputStream())).readLine());

        Part filePart = request.getPart("file");

        FileDto uploadedFileInfo = FileDto.builder()
                .size(filePart.getSize())
//                .description(description)
                .mimeType(filePart.getContentType())
                .fileName(filePart.getSubmittedFileName())
                .fileStream(filePart.getInputStream())
                .build();

        String storageFileName = filesService.upload(uploadedFileInfo);

        User user = usersService.getUserByEmail((String) request.getSession().getAttribute("email"));
        user.setImgId(filesRepository.findByStorageFileName(storageFileName).orElseThrow().getId());
        usersService.update(user);

        response.sendRedirect(APPLICATION_PREFIX + USER_PROFILE_PATH);
    }
}

