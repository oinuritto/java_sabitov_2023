package ru.kpfu.itis.oinuritto.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static ru.kpfu.itis.oinuritto.constants.Paths.APPLICATION_PREFIX;
import static ru.kpfu.itis.oinuritto.constants.Paths.HELLO_PAGE;

@WebServlet("/logout")
public class LogoutServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession httpSession = req.getSession(false);
        if (httpSession != null) {
            httpSession.removeAttribute("authenticated");
        }

        resp.sendRedirect(APPLICATION_PREFIX + HELLO_PAGE);
    }
}
