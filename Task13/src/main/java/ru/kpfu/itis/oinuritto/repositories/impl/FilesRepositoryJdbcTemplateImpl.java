package ru.kpfu.itis.oinuritto.repositories.impl;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import ru.kpfu.itis.oinuritto.models.FileInfo;
import ru.kpfu.itis.oinuritto.repositories.FilesRepository;

import javax.sql.DataSource;
import java.util.*;
import java.util.stream.Collectors;

@Repository
public class FilesRepositoryJdbcTemplateImpl implements FilesRepository {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public FilesRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    //language=SQL
    private final static String SQL_SELECT_BY_STORAGE_NAME = "select * from file_info where storage_file_name = :storageFileName";

    //language=SQL
    private final static String SQL_SELECT_BY_TYPES = "select * from file_info where mime_type in (:types)";

    //language=SQL
    private final static String SQL_SELECT_BY_ID = "select * from file_info where id = :id";

    private final static RowMapper<FileInfo> fileInfoRowMapper = (row, rowNumber) -> FileInfo.builder()
            .id(row.getLong("id"))
            .originalFileName(row.getString("original_file_name"))
            .storageFileName(row.getString("storage_file_name"))
            .size(row.getLong("size"))
            .mimeType(row.getString("mime_type"))
            .description(row.getString("description"))
            .build();

    @Override
    public void save(FileInfo fileInfo) {
        Map<String, Object> params = new HashMap<>();
        params.put("originalFileName", fileInfo.getOriginalFileName());
        params.put("storageFileName", fileInfo.getStorageFileName());
        params.put("size", fileInfo.getSize());
        params.put("mimeType", fileInfo.getMimeType());
        params.put("description", fileInfo.getDescription());

        SimpleJdbcInsert insert = new SimpleJdbcInsert(namedParameterJdbcTemplate.getJdbcTemplate());

        Long id = insert.withTableName("file_info")
                .usingGeneratedKeyColumns("id")
                .executeAndReturnKey(new MapSqlParameterSource(params)).longValue();


        fileInfo.setId(id);
    }

    @Override
    public Optional<FileInfo> findByStorageFileName(String fileName) {
        try {
            return Optional.of(namedParameterJdbcTemplate.queryForObject(SQL_SELECT_BY_STORAGE_NAME,
                    Collections.singletonMap("storageFileName", fileName), fileInfoRowMapper));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public Optional<FileInfo> findById(Long id) {
        try {
            return Optional.of(namedParameterJdbcTemplate.queryForObject(SQL_SELECT_BY_ID,
                    Collections.singletonMap("id", id), fileInfoRowMapper));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public List<String> findAllStorageNamesByType(String... types) {
        List<String> typesAsArray = Arrays.stream(types).toList();
        List<FileInfo> files = namedParameterJdbcTemplate.query(SQL_SELECT_BY_TYPES,
                Collections.singletonMap("types", typesAsArray), fileInfoRowMapper);
        return files.stream().map(FileInfo::getStorageFileName).collect(Collectors.toList());
    }
}

