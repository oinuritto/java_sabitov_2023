package ru.kpfu.itis.oinuritto.services;

import ru.kpfu.itis.oinuritto.dto.SignUpDto;
import ru.kpfu.itis.oinuritto.dto.UserDto;
import ru.kpfu.itis.oinuritto.models.User;

import java.util.List;

public interface UsersService {
    void signUp(SignUpDto signUpData);
    List<UserDto> getAllUsers();

    List<User> getAllUsersByAge(int ageFrom, int ageTo);

    User getUserByEmail(String email);

    void update(User user);
}
