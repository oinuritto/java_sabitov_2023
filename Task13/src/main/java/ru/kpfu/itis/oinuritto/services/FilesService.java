package ru.kpfu.itis.oinuritto.services;


import ru.kpfu.itis.oinuritto.dto.FileDto;

public interface FilesService {
    String upload(FileDto file);

    FileDto getFile(String fileName);

    FileDto getFile(Long id);
}
