package ru.kpfu.itis.oinuritto.security;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.kpfu.itis.oinuritto.models.User;
import ru.kpfu.itis.oinuritto.repositories.UsersRepository;

@RequiredArgsConstructor
@Component
public class AuthenticationManagerImpl implements AuthenticationManager {

    private final UsersRepository usersRepository;

    @Override
    public boolean authenticate(String email, String password) {
        User user = usersRepository.findOneByEmail(email).orElse(null);

        if (user == null) {
            return false;
        }

        return user.getPassword().equals(password);
    }
}
