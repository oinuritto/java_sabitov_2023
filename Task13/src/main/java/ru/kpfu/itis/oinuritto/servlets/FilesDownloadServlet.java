package ru.kpfu.itis.oinuritto.servlets;

import org.springframework.context.ApplicationContext;
import ru.kpfu.itis.oinuritto.dto.FileDto;
import ru.kpfu.itis.oinuritto.services.FilesService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import static ru.kpfu.itis.oinuritto.constants.Paths.*;

@WebServlet(urlPatterns = {FILES_PATH, IMAGES_PATH})
@MultipartConfig
public class FilesDownloadServlet extends HttpServlet {

    private FilesService filesService;
    private List<String> imageTypes;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.filesService = context.getBean(FilesService.class);
        imageTypes = new ArrayList<>(List.of("image/jpeg", "image/png", "image/jpg"));
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        FileDto file;
        if (request.getRequestURI().equals(APPLICATION_PREFIX + FILES_PATH)) {
            String storageFileName = request.getParameter("fileName");
            file = filesService.getFile(storageFileName);
        } else {
            Long id = Long.parseLong(request.getParameter("id"));
            file = filesService.getFile(id);
            if (!imageTypes.contains(file.getMimeType())) {
                response.sendRedirect(APPLICATION_PREFIX + FILES_UPLOAD_PATH + "?error");
            }
        }

        response.setContentType(file.getMimeType());
        response.setContentLength(file.getSize().intValue());
        response.setHeader("Content-Disposition", "filename=\"" + file.getOriginalFileName() + "\"");
        Files.copy(file.getPath(), response.getOutputStream());
        response.flushBuffer();
    }
}


