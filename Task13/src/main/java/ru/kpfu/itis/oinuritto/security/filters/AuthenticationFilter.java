package ru.kpfu.itis.oinuritto.security.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static ru.kpfu.itis.oinuritto.constants.Paths.*;

@WebFilter("/*")
public class AuthenticationFilter implements Filter {
    public static String fromURI;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        if (request.getRequestURI().equals(APPLICATION_PREFIX + SIGN_IN_PATH) ||
                request.getRequestURI().equals(APPLICATION_PREFIX + SIGN_UP_PAGE) ||
                request.getRequestURI().equals(APPLICATION_PREFIX + HELLO_PAGE) ||
                request.getRequestURI().equals(APPLICATION_PREFIX + SIGN_UP_PATH)) {
            filterChain.doFilter(request, response);
            return;
        }

        fromURI = request.getRequestURI();
        System.out.println(fromURI);

        if (isAuthenticated(request)) {
            filterChain.doFilter(request, response);
            return;
        }

        response.sendRedirect(APPLICATION_PREFIX + SIGN_IN_PATH);
    }

    private static boolean isAuthenticated(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session != null) {
            Boolean authenticated = (Boolean) session.getAttribute("authenticated");
            return (authenticated != null && authenticated);
        }
        return false;
    }
}
