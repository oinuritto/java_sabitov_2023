package ru.kpfu.itis.oinuritto.servlets;

import org.springframework.context.ApplicationContext;
import ru.kpfu.itis.oinuritto.models.User;
import ru.kpfu.itis.oinuritto.services.UsersService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static ru.kpfu.itis.oinuritto.constants.Paths.USER_PROFILE_PATH;

@WebServlet(USER_PROFILE_PATH)
public class UserProfileServlet extends HttpServlet {
    private UsersService usersService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.usersService = context.getBean(UsersService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = usersService.getUserByEmail((String) req.getSession().getAttribute("email"));
        req.setAttribute("user", user);
        req.getServletContext().getRequestDispatcher("/userProfile.jsp").forward(req, resp);
    }
}
