package ru.kpfu.itis.oinuritto.repositories;

import ru.kpfu.itis.oinuritto.models.User;

import java.util.List;
import java.util.Optional;

public interface UsersRepository {
    List<User> findAll();

    void save(User user);

    Optional<User> findById(Long id);

    void update(User user);

    void delete(Long id);

    List<User> findAllByAgeInRangeOrderByIdDesc(int minAge, int maxAge);

    List<User> findAllByFirstNameOrLastNameLike(String query);

    Optional<User> findOneByEmail(String email);
}
