package ru.kpfu.itis.oinuritto.repositories;

import ru.kpfu.itis.oinuritto.models.FileInfo;

import java.util.List;
import java.util.Optional;

public interface FilesRepository {
    void save(FileInfo fileInfo);

    Optional<FileInfo> findByStorageFileName(String fileName);

    Optional<FileInfo> findById(Long id);

    List<String> findAllStorageNamesByType(String ... types);
}

