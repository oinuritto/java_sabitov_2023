package ru.kpfu.itis.oinuritto.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static ru.kpfu.itis.oinuritto.constants.Paths.*;

@WebServlet(urlPatterns = {PRODUCTS_SORT_CHANGE_PATH}, loadOnStartup = 0)
public class ProductsSortingCookieServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String sortBy = req.getParameter("sortBySelect");
        Cookie sortingCookie = new Cookie("sortBy", sortBy);
        sortingCookie.setPath("/");
        resp.addCookie(sortingCookie);
        resp.sendRedirect(APPLICATION_PREFIX + PRODUCTS_PATH);
    }
}
