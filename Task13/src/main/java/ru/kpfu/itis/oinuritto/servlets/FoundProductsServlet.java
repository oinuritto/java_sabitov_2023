package ru.kpfu.itis.oinuritto.servlets;

import org.springframework.context.ApplicationContext;
import ru.kpfu.itis.oinuritto.models.Product;
import ru.kpfu.itis.oinuritto.services.ProductsService;
import ru.kpfu.itis.oinuritto.utils.HtmlGenerator;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import static ru.kpfu.itis.oinuritto.constants.Paths.APPLICATION_PREFIX;
import static ru.kpfu.itis.oinuritto.constants.Paths.FOUND_PRODUCTS_PATH;

@WebServlet(FOUND_PRODUCTS_PATH)
public class FoundProductsServlet extends HttpServlet {
    private ProductsService productsService;
    private HtmlGenerator htmlGenerator;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.productsService = context.getBean(ProductsService.class);
        this.htmlGenerator = context.getBean(HtmlGenerator.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getRequestURI().equals(APPLICATION_PREFIX + FOUND_PRODUCTS_PATH)
                && req.getParameter("productName") != null) {
            List<Product> products;
            String productName = req.getParameter("productName").trim();

            products = productsService.getProductLikeName(productName);

            resp.setContentType("text/html");
            PrintWriter writer = resp.getWriter();

            String html = htmlGenerator.getHtmlForProducts(products);

            writer.println(html);
        } else {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }
}
