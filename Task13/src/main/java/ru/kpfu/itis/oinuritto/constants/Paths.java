package ru.kpfu.itis.oinuritto.constants;

public class Paths {
    public static final String APPLICATION_PREFIX = "/app";

    public static final String ADD_PRODUCT_PATH = "/addProduct";

    public static final String PRODUCTS_PATH = "/products";

    public static final String FOUND_PRODUCTS_PATH = "/foundProducts";

    public static final String PRODUCTS_LIVE_SEARCH_PAGE = "/liveSearchProduct.html";

    public static final String PRODUCTS_LIVE_SEARCH_PATH = "/liveSearchProduct";

    public static final String PRODUCTS_SORT_CHANGE_PATH = "/products/sortChange";

    public static final String SIGN_UP_PATH = "/signUp";

    public static final String SIGN_UP_PAGE = "/signUp.html";

    public static final String SIGN_IN_PATH = "/signIn";

    public static final String SIGN_IN_PAGE = "/signIn.html";

    public static final String PROFILE_PAGE = "/profile.html";

    public static final String PROFILE_PATH = "/profile";
    public static final String HELLO_PAGE = "/hello.html";

    public static final String FILES_UPLOAD_PATH = "/files/upload";
//    public static final String FILES_UPLOAD_PATH = "/user/setProfileImage";

    public static final String FILES_PATH = "/files";

    public static final String IMAGES_PATH = "/images";

    public static final String USER_PROFILE_PATH = "/profile/user";


}
