package ru.itis.taxi.app;

import ru.itis.taxi.dto.SignUpForm;
import ru.itis.taxi.mappers.Mappers;
import ru.itis.taxi.models.User;
import ru.itis.taxi.repositories.UsersRepository;
import ru.itis.taxi.repositories.UsersRepositoryFilesImpl;
import ru.itis.taxi.services.UsersService;
import ru.itis.taxi.services.UsersServiceImpl;

import java.util.List;
import java.util.UUID;

public class Main {

    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFilesImpl("users.txt");
        UsersService usersService = new UsersServiceImpl(usersRepository, Mappers::fromSignUpForm);

        usersService.signUp(new SignUpForm("Марсель", "Сидиков",
                "sidikov.marsel@gmail.com", "qwerty007"));

        usersService.signUp(new SignUpForm("Айнур", "Сабитов",
                "ainursabitovak@mail.ru", "qwerty007700"));

        usersService.signUp(new SignUpForm("Иван", "Иванов",
                "ivanov@mail.ru", "qwerty700"));

        usersService.signUp(new SignUpForm("Максим", "Максимов",
                "maximov@mail.ru", "qwertyasd007"));

        // check findAll()
        List<User> userList = usersRepository.findAll();
        System.out.println(userList);

        // check findById();
        System.out.println(usersRepository.findById(userList.get(0).getId()));

        // check deleteById()
        usersRepository.deleteById(userList.get(0).getId());

        // check delete()
        usersRepository.delete(userList.get(1));

        // check update()
        User user = userList.get(2);
        User updatedUser = new User(user.getId(), user.getFirstName(), user.getLastName(), "newmail@gmail.com", "newpassword");
        usersRepository.update(updatedUser);

        int i = 0;
    }
}
