package ru.itis.taxi.models;

import java.util.StringJoiner;
import java.util.UUID;

/**
 * 30.06.2022
 * 02. TaxiService
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class User {
    private UUID id;
    private final String firstName;
    private final String lastName;
    private final String email;
    private final String password;

    public User(String firstName, String lastName, String email, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
    }

    public User(UUID id, String firstName, String lastName, String email, String password) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        if (id != null) {
            return new StringJoiner(", ", User.class.getSimpleName() + "[", "]")
                    .add("id=" + id.toString())
                    .add("firstName='" + firstName + "'")
                    .add("lastName='" + lastName + "'")
                    .add("email='" + email + "'")
                    .toString();
        } else {
            return new StringJoiner(", ", User.class.getSimpleName() + "[", "]")
                    .add("id=" + id)
                    .add("firstName='" + firstName + "'")
                    .add("lastName='" + lastName + "'")
                    .add("email='" + email + "'")
                    .toString();
        }
    }
}
