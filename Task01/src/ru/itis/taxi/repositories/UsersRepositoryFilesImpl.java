package ru.itis.taxi.repositories;

import ru.itis.taxi.models.User;

import java.io.*;
import java.util.*;
import java.util.function.Function;

/**
 * 30.06.2022
 * 02. TaxiService
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class UsersRepositoryFilesImpl implements UsersRepository {

    private final String fileName;

    private static final Function<User, String> userToString = user ->
            user.getId().toString()
                    + "|" + user.getFirstName()
                    + "|" + user.getLastName()
                    + "|" + user.getEmail()
                    + "|" + user.getPassword();

    public UsersRepositoryFilesImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<User> findAll() {
        List<User> userList = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            while (reader.ready()) {
                String line = reader.readLine();
                String[] userInfoArr = line.split("\\|");
                if (userInfoArr.length == 5) {
                    userList.add(new User(
                            UUID.fromString(userInfoArr[0]),
                            userInfoArr[1],
                            userInfoArr[2],
                            userInfoArr[3],
                            userInfoArr[4]));
                }
            }
            return userList;
        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public void save(User entity) {
        if (entity.getId() == null) {
            entity.setId(UUID.randomUUID());
        }

        try (Writer writer = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {

            String userAsString = userToString.apply(entity);
            bufferedWriter.write(userAsString);
            bufferedWriter.newLine();

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(User entity) {
        List<User> userList = findAll();
        int index = 0;
        boolean flag = false;
        for (int i = 0; i < userList.size(); i++) {
            if (userList.get(i).getId().equals(entity.getId())) {
                index = i;
                flag = true;
                break;
            }
        }
        if (flag) {
            userList.set(index, entity);
            writeUsersListToFile(userList);
        }
    }

    @Override
    public void delete(User entity) {
        UUID deletingId = entity.getId();
        deleteById(deletingId);
    }

    @Override
    public void deleteById(UUID id) {
        List<User> userList = findAll();
        userList.removeIf(user -> user.getId().equals(id));
        writeUsersListToFile(userList);
    }

    @Override
    public User findById(UUID id) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            while (reader.ready()) {
                String line = reader.readLine();
                if (line.contains(id.toString())) {
                    String[] userInfoArr = line.split("\\|");
                    return new User(
                            UUID.fromString(userInfoArr[0]),
                            userInfoArr[1],
                            userInfoArr[2],
                            userInfoArr[3],
                            userInfoArr[4]);
                }
            }
        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
        return null;
    }

    private void writeUsersListToFile(List<User> userList) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, false))) {
            for (User user: userList) {
                String userAsString = userToString.apply(user);
                writer.write(userAsString);
                writer.newLine();
            }
            writer.flush();
        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }
}
