package ru.itis.taxi.services;

import ru.itis.taxi.dto.SignUpForm;

/**
 * 30.06.2022
 * 02. TaxiService
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface UsersService {
    void signUp(SignUpForm signUpForm);
}
