insert into car(car_model, car_number)
values ('BMW', 'к000кк97'),
       ('Mercedes', 'у999уу116'),
       ('Lada', 'в777вв77');

insert into driver(first_name, last_name, rating)
values ('Айнур', 'Сабитов', 5),
       ('Алишер', 'Хабибуллин', 4.9),
       ('Нурислам', 'Зарипов', 4.87);

insert into client(first_name, last_name, phone_number)
values ('Антон', 'Антонов', '81149984456'),
       ('Иван', 'Иванов', '89178992345'),
       ('Андрей', 'Андреев', '');

update client
set phone_number = '79377777777'
where id = 3;

update driver
set car_id = 3
where id = 2;

update driver
set car_id = 1
where id = 1;

update driver
set car_id = 2
where id = 3;

insert into taxi_order(address_from, address_to, cost, client_id, driver_id)
values ('Антоновка', 'Ивановка', 3000, 1, 3),
       ('Ивановка', 'Антоновка', 2997, 3, 1),
       ('Вишневка', 'Антоновка', 2675, 2 ,2);
