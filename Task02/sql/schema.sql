DROP table if exists client cascade ;
DROP table if exists driver cascade ;
DROP table if exists car cascade ;
drop table if exists taxi_order ;

create table client
(
    id         bigserial primary key,
    first_name char(20) not null ,
    last_name  char(20)
);

create table driver
(
    id          serial primary key,
    first_name char(20) not null,
    last_name  char(20),
    rating float check (rating > 0 and rating <= 5) not null default 5
);

create table car
(
    id serial primary key,
    car_model char(20),
    car_number char(20)
);

create table taxi_order
(
    id serial primary key,
    address_from char(30),
    address_to char(30),
    cost int,
    client_id bigint,
    driver_id bigint
);

alter table client
    add phone_number char(20) not null default '';

alter table driver add car_id bigint;

alter table driver add foreign key(car_id) references car(id);

alter table taxi_order add foreign key(client_id) references client(id);

alter table taxi_order add foreign key(driver_id) references driver(id);
