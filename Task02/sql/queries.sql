select *
from client
order by first_name;

select car_model, car_number
from car
order by car_model;

select address_from, address_to, cost
from taxi_order
order by cost desc;