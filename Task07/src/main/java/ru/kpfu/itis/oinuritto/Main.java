package ru.kpfu.itis.oinuritto;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.kpfu.itis.oinuritto.models.Product;
import ru.kpfu.itis.oinuritto.repositories.ProductsRepository;
import ru.kpfu.itis.oinuritto.repositories.ProductsRepositoryJdbcTemplateImpl;

import java.io.IOException;
import java.util.Properties;

public class Main {
    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(Main.class.getResourceAsStream("/db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(properties.getProperty("db.url"));
        config.setUsername(properties.getProperty("db.username"));
        config.setPassword(properties.getProperty("db.password"));
        config.setMaximumPoolSize(20);

        HikariDataSource dataSource = new HikariDataSource(config);

        ProductsRepository productsRepository = new ProductsRepositoryJdbcTemplateImpl(dataSource);

        // checks
        // findAll()
        System.out.println(productsRepository.findAll());

        // findById()
        System.out.println(productsRepository.findById(2L).orElseThrow(IllegalArgumentException::new));

        // save()
        Product prod1 = Product.builder()
                .productName("product1")
                .price(340)
                .count(142)
                .color("Pink")
                .build();

        productsRepository.save(prod1);
        System.out.println(productsRepository.findAll());

        // findAllByPriceGreaterThanOrderByIdDesc()
        System.out.println(productsRepository.findAllByPriceGreaterThanOrderByIdDesc(300));

        // update()
        System.out.println(productsRepository.findById(2L));
        Product bucketUpdated = Product.builder()
                .id(2L)
                .productName("Bucket")
                .price(58)
                .count(330)
                .color("Black")
                .build();
        productsRepository.update(bucketUpdated);
        System.out.println(productsRepository.findById(2L));

        // delete()
        productsRepository.delete(5L);
        System.out.println(productsRepository.findAll());
    }
}
