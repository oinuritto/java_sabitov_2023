package ru.kpfu.itis.oinuritto.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class Product {
    private Long id;
    private String productName;
    private Integer price;
    private Integer count;
    private String color;
}
