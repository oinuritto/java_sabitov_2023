package ru.kpfu.itis.oinuritto.models;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@ToString
public class Product {
    private Long id;
    private String productName;
    private Integer price;
    private Integer count;
    private String color;
}
