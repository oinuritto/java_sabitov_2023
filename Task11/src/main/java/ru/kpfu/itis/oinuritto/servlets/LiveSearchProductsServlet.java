package ru.kpfu.itis.oinuritto.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.ApplicationContext;
import ru.kpfu.itis.oinuritto.dto.ProductDto;
import ru.kpfu.itis.oinuritto.services.SearchService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static ru.kpfu.itis.oinuritto.constants.Paths.PRODUCTS_LIVE_SEARCH_PATH;

@WebServlet(name = "searchServlet", urlPatterns = {PRODUCTS_LIVE_SEARCH_PATH}, loadOnStartup = 1)
public class LiveSearchProductsServlet extends HttpServlet {
    private SearchService searchService;
    private ObjectMapper objectMapper;

    @Override
    public void init(ServletConfig config) {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.searchService = context.getBean(SearchService.class);
        this.objectMapper = context.getBean(ObjectMapper.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String query = request.getParameter("query");
        List<ProductDto> users = searchService.searchProducts(query);
        String jsonResponse = objectMapper.writeValueAsString(users);
        response.setContentType("application/json");
        response.getWriter().write(jsonResponse);
    }
}
