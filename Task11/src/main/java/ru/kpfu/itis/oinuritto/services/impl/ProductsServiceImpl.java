package ru.kpfu.itis.oinuritto.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.oinuritto.dto.ProductDto;
import ru.kpfu.itis.oinuritto.models.Product;
import ru.kpfu.itis.oinuritto.repositories.ProductsRepository;
import ru.kpfu.itis.oinuritto.services.ProductsService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductsServiceImpl implements ProductsService {
    private final ProductsRepository productsRepository;

    @Override
    public boolean addProduct(ProductDto productData) {
        try {
            Product product = Product.builder()
                    .productName(productData.getProductName())
                    .price(productData.getPrice())
                    .count(productData.getCount())
                    .color(productData.getColor())
                    .build();

            productsRepository.save(product);
            return true;
        } catch (DataIntegrityViolationException e) {
            return false;
        }
    }

    @Override
    public List<Product> getAllProducts() {
        return productsRepository.findAll();
    }

    @Override
    public boolean deleteProduct(Long id) {
        return productsRepository.delete(id) != 0;
    }

    @Override
    public List<Product> getProductLikeName(String productName) {
        return productsRepository.findProductsLikeName(productName.toLowerCase());
    }
}
