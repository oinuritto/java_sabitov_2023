package ru.kpfu.itis.oinuritto.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.oinuritto.dto.ProductDto;
import ru.kpfu.itis.oinuritto.repositories.ProductsRepository;
import ru.kpfu.itis.oinuritto.services.SearchService;

import java.util.Collections;
import java.util.List;

import static ru.kpfu.itis.oinuritto.dto.ProductDto.from;


@RequiredArgsConstructor
@Service
public class SearchServiceImpl implements SearchService {
    private final ProductsRepository productsRepository;

    @Override
    public List<ProductDto> searchProducts(String query) {
        if (query == null || query.equals("")) {
            return Collections.emptyList();
        }
        return from(productsRepository.findProductsLikeName(query));

    }
}
