package ru.kpfu.itis.oinuritto.dto.converters.http;

import ru.kpfu.itis.oinuritto.dto.ProductDto;

import javax.servlet.http.HttpServletRequest;

public class HttpFormsConverter {
    public static ProductDto from(HttpServletRequest request) {
        return ProductDto.builder()
                .productName(request.getParameter("productName"))
                .price(Integer.parseInt(request.getParameter("price")))
                .count(Integer.parseInt(request.getParameter("count")))
                .color(request.getParameter("color"))
                .build();
    }

}
