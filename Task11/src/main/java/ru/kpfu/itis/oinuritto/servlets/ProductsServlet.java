package ru.kpfu.itis.oinuritto.servlets;

import org.springframework.context.ApplicationContext;
import ru.kpfu.itis.oinuritto.dto.ProductDto;
import ru.kpfu.itis.oinuritto.dto.converters.http.HttpFormsConverter;
import ru.kpfu.itis.oinuritto.models.Product;
import ru.kpfu.itis.oinuritto.services.ProductsService;
import ru.kpfu.itis.oinuritto.utils.HtmlGenerator;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import static ru.kpfu.itis.oinuritto.constants.Paths.*;

@WebServlet(urlPatterns = {PRODUCTS_PATH, ADD_PRODUCT_PATH}, loadOnStartup = 1)
public class ProductsServlet extends HttpServlet {
    private ProductsService productsService;
    private HtmlGenerator htmlGenerator;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.productsService = context.getBean(ProductsService.class);
        this.htmlGenerator = context.getBean(HtmlGenerator.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getRequestURI().equals(APPLICATION_PREFIX + PRODUCTS_PATH)) {
            List<Product> products;

            products = productsService.getAllProducts();

            resp.setContentType("text/html");
            PrintWriter writer = resp.getWriter();

            String html = htmlGenerator.getHtmlForProducts(products);

            writer.println(html);
        } else {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getRequestURI().equals(APPLICATION_PREFIX + ADD_PRODUCT_PATH)) {
            ProductDto productDto = HttpFormsConverter.from(req);

            productsService.addProduct(productDto);
            resp.sendRedirect(APPLICATION_PREFIX + PRODUCTS_PATH);
        } else {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }

    }
}
