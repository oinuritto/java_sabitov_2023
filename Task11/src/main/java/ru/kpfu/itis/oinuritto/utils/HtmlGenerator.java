package ru.kpfu.itis.oinuritto.utils;

import org.springframework.stereotype.Component;
import ru.kpfu.itis.oinuritto.models.Product;

import java.util.List;

@Component
public class HtmlGenerator {
    public String getHtmlForProducts(List<Product> products) {
        StringBuilder html = new StringBuilder();

        html.append("<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "\t<meta charset=\"utf-8\">\n" +
                "\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                "\t<title>Products</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<table>\n" +
                "\t<tr>\n" +
                "\t\t<th>ID</th>\n" +
                "\t\t<th>Product Name</th>\n" +
                "\t\t<th>Price</th>\n" +
                "\t\t<th>Count</th>\n" +
                "\t\t<th>Color</th>\n" +
                "\t</tr>");

        for (Product product : products) {
            html.append("<tr>\n");
            html.append("<td>").append(product.getId()).append("</td>\n");
            html.append("<td>").append(product.getProductName()).append("</td>\n");
            html.append("<td>").append(product.getPrice()).append("</td>\n");
            html.append("<td>").append(product.getCount()).append("</td>\n");
            html.append("<td>").append(product.getColor()).append("</td>\n");
            html.append("</tr>\n");
        }

        html.append("</table>\n" +
                "</body>\n" +
                "</html>");
        return html.toString();
    }
}
