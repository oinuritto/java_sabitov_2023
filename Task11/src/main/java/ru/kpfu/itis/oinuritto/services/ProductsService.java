package ru.kpfu.itis.oinuritto.services;

import ru.kpfu.itis.oinuritto.dto.ProductDto;
import ru.kpfu.itis.oinuritto.models.Product;

import java.util.List;

public interface ProductsService {
    boolean addProduct(ProductDto productData);
    List<Product> getAllProducts();
    boolean deleteProduct(Long id);

    List<Product> getProductLikeName(String productName);
}
