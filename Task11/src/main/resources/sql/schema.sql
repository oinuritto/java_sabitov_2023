drop table if exists product;

create table product
(
    id         bigserial primary key,
    product_name varchar(20),
    price int,
    count int check ( count >= 0 ),
    color varchar(20)
);

-- чтобы поиск не зависел от регистра введенных данных
create extension citext;
