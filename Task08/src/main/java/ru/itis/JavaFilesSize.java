package ru.itis;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Mojo(name = "java-files-size", defaultPhase = LifecyclePhase.COMPILE)
public class JavaFilesSize extends AbstractMojo {

    @Parameter(defaultValue = "${project.build.outputDirectory}", required = true)
    private String outputFolderFileName;

    @Parameter(defaultValue = "${project.build.sourceDirectory}", required = true, readonly = true)
    private String sourceFolderFileName;

    @Parameter(name = "javaFilesSizeFileName", required = true)
    private String javaFilesSizeFileName;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        File outputFolder = new File(outputFolderFileName);

        File listOfClassesFile = new File(outputFolder, javaFilesSizeFileName);

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(listOfClassesFile))) {
            getLog().info("Output file for sizes of java files is - " + javaFilesSizeFileName);

            Files.walk(Paths.get(sourceFolderFileName))
                    .filter(Files::isRegularFile)
                    .forEach(file -> {
                        try {
                            writer.write( file.getFileName().toString() + " - " + sizeCut(file.toFile().length()));
                            writer.newLine();
                        } catch (IOException e) {
                            throw new IllegalArgumentException(e);
                        }
                    });

            getLog().info("Finish work");

        } catch (IOException e) {
            throw new MojoExecutionException(e);
        }

    }

    private String sizeCut(long size) {
        int i = 0;
        String[] sizeCutsNames = {"B", "KB", "MB", "GB", "TB"};
        while (size >= 1024) {
            size /= 1024;
            i++;
            if (i == 4) {
                break;
            }
        }
        return size + " " + sizeCutsNames[i];
    }
}
