package ru.kpfu.itis.oinuritto.services.impl;

import org.springframework.dao.DataIntegrityViolationException;
import ru.kpfu.itis.oinuritto.models.Product;
import ru.kpfu.itis.oinuritto.repositories.ProductsRepository;
import ru.kpfu.itis.oinuritto.services.ProductsService;

import java.util.List;

public class ProductsServiceImpl implements ProductsService {
    private final ProductsRepository productsRepository;

    public ProductsServiceImpl(ProductsRepository productsRepository) {
        this.productsRepository = productsRepository;
    }


    @Override
    public boolean addProduct(String productName, Integer price, Integer count, String color) {
        try {
            Product product = Product.builder()
                    .productName(productName)
                    .price(price)
                    .count(count)
                    .color(color)
                    .build();

            productsRepository.save(product);
            return true;
        } catch (DataIntegrityViolationException e) {
            return false;
        }
    }

    @Override
    public List<Product> findAllProducts() {
        return productsRepository.findAll();
    }

    @Override
    public boolean deleteProduct(Long id) {
        return productsRepository.delete(id) != 0;
    }
}
