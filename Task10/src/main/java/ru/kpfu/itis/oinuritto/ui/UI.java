package ru.kpfu.itis.oinuritto.ui;

import ru.kpfu.itis.oinuritto.models.Product;
import ru.kpfu.itis.oinuritto.services.ProductsService;

import java.util.List;
import java.util.Scanner;

public class UI {

    private final Scanner scanner = new Scanner(System.in);

    private final ProductsService productsService;

    public UI(ProductsService productsService) {
        this.productsService = productsService;
    }

    public void start() {
        while (true) {
            printMainMenu();

            String command = scanner.nextLine();

            switch (command) {
                case "1" -> addProductCommand();
                case "2" -> listOfProductsCommand();
                case "3" -> deleteProductCommand();
                case "4" -> System.exit(0);
                default -> System.out.println("Команда не распознана");
            }
        }


    }

    private void deleteProductCommand() {
        System.out.print("ID: ");
        // если брать scanner.nextLong(), то происходит какое-то двойное считывание с консоли :(
        Long id = Long.parseLong(scanner.nextLine());
        if (productsService.deleteProduct(id)) {
            System.out.println("Товар под id = " + id + " успешно удалён");
        } else {
            System.out.println("Проблемы с удалением товара. Попробуйте ввести другой id");
        }
    }

    private void listOfProductsCommand() {
        System.out.println("Список товаров:");
        // System.out.println(productsService.findAllProducts());
        List<Product> productList = productsService.findAllProducts();
        for (Product product: productList) {
            System.out.println(product.toString());
        }
    }

    private void addProductCommand() {
        System.out.print("Название товара: ");
        String productName = scanner.nextLine();

        System.out.print("Цена: ");
        Integer price = scanner.nextInt();

        System.out.print("Количество: ");
        Integer count = scanner.nextInt();

        System.out.print("Цвет: ");
        scanner.nextLine();
        String color = scanner.nextLine();

        if (productsService.addProduct(productName, price, count, color)) {
            System.out.println("Товар добавлен.");
        } else {
            System.out.println("Проблемы с добавлением товара");
        }
    }

    private void printMainMenu() {
        System.out.println("Выберите действие:");
        System.out.println("1. Добавить товар");
        System.out.println("2. Посмотреть список товаров");
        System.out.println("3. Удалить товар по ID");
        System.out.println("4. Выход");
    }
}

