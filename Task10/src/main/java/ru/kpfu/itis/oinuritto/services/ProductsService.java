package ru.kpfu.itis.oinuritto.services;

import ru.kpfu.itis.oinuritto.models.Product;

import java.util.List;

public interface ProductsService {
    boolean addProduct(String productName, Integer price, Integer count, String color);
    List<Product> findAllProducts();
    boolean deleteProduct(Long id);
}
