package ru.kpfu.itis.oinuritto.repositories;

import ru.kpfu.itis.oinuritto.models.Product;

import java.util.List;
import java.util.Optional;

public interface ProductsRepository {
    Optional<Product> findById(Long id);
    void update(Product product);
    int delete(Long id);
    List<Product> findAll();
    void save(Product product);
    List<Product> findAllByPriceGreaterThanOrderByIdDesc(int minPrice);
}
