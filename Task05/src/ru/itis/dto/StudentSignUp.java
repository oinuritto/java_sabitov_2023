package ru.itis.dto;

/**
 * 11.07.2022
 * 03. Database
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class StudentSignUp {

    public StudentSignUp(String email, String password, String firstName, String lastName) {
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    private String email;
    private String password;
    private String firstName;
    private String lastName;

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
